package payme

type ExceptionResponse struct {
	Error ErrorResponse `json:"error"`
	Id    int32         `json:"id"`
}

type ErrorResponse struct {
	Code    int32       `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func generateException(id, code int32, message string, data interface{}) ExceptionResponse {
	return ExceptionResponse{
		Error: ErrorResponse{
			Code:    code,
			Message: message,
			Data:    data,
		},
		Id: id,
	}
}

func OrderNotExistsException(id int32) ExceptionResponse {
	return generateException(id, -31050, "Order not found", "order")
}

func WrongAmountException(id int32) ExceptionResponse {
	return generateException(id, -31001, "Wrong Amount", "amount")
}

func UnableCompleteException(id int32) ExceptionResponse {
	return generateException(id, -31008, "Unable to complete operation", "transaction")
}

func TransactionNotFoundException(id int32) ExceptionResponse {
	return generateException(id, -31003, "Transaction not found", "transaction")
}

func UnableCancelTransactionException(id int32) ExceptionResponse {
	return generateException(id, -31007, "Unable to cancel transaction", "transaction")
}

func InternalServerErrorException(id int32) ExceptionResponse {
	return generateException(id, -32400, "Internal Server Error", "error")
}

func UnathorizedRequest(id int32) ExceptionResponse {
	return generateException(id, -32504, "Unathorized request", "")
}

func MethodNotFound(id int32) ExceptionResponse {
	return generateException(id, -32601, "RPC Method is wrong", "")
}

