package payme

const (
	TIME_EXPIRES int64 = 4320000

	STATE_IN_PROGRESS   int32 = 1
	STATE_DONE          int32 = 2
	STATE_CANCELED      int32 = -1
	STATE_POST_CANCELED int32 = -2
)

