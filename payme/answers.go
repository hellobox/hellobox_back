package payme

import (
	"encoding/base64"
	"fmt"
	"hellobox/models"
)

func CheckPerformTransactionAnswer(allow bool) models.PaymeResponse {
	return models.PaymeResponse{
		Result: models.CheckPerformTransactionAnswer{
			Allow: allow,
		},
	}
}

func CreateTransactionAnswer(createTime int64, transaction string, state int32, receivers []models.Receivers) models.PaymeResponse {
	return models.PaymeResponse{
		Result: models.CreateTransactionAnswer{
			CreateTime:  createTime,
			Transaction: transaction,
			State:       state,
			Receivers:   receivers,
		},
	}
}

func PerformTransactionAnswer(transaction string, state int32, performTime int64) models.PaymeResponse {
	return models.PaymeResponse{
		Result: models.PerformTransactionAnswer{
			Transaction: transaction,
			State:       state,
			PerformTime: performTime,
		},
	}
}

func CancelTransactionAnswer(transaction string, state int32, cancelTime int64) models.PaymeResponse {
	return models.PaymeResponse{
		Result: models.CancelTransactionAnswer{
			Transaction: transaction,
			State:       state,
			CancelTime:  cancelTime,
		},
	}
}

func CheckTransactionAnswer(transaction string, state int32, createTime int64, performTime int64, cancelTime int64, reason *int64) models.PaymeResponse {
	return models.PaymeResponse{
		Result: models.CheckTransactionAnswer{
			Transaction: transaction,
			State:       state,
			CreateTime:  createTime,
			PerformTime: performTime,
			CancelTime:  cancelTime,
			Reason:      reason,
		},
	}
}

func TransactionsAnswer(result interface{}) models.PaymeResponse {
	return models.PaymeResponse{
		Result: result,
	}
}

func GeneratePaymeLink(orderId int32, amount int64, merchant_id string) string {
	amount = amount * 100

	params := fmt.Sprintf("m=%s;ac.order_id=%d;ac.amount=%d", merchant_id, orderId, amount)

	// TODO: get from config
	paymeCheckoutUrl := "https://checkout.paycom.uz/"

	params = base64.StdEncoding.EncodeToString([]byte(params))

	return paymeCheckoutUrl + params
}

