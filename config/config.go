package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

// local 5581974011:AAFIFH5vS26q6f5G06N-Jgcdb2DIFyX1JQY // my token
// local 575823245:AAGW92p0-LiEmE8Nz6d7_5eh7RoFRM4XS0k // mirsultan
// prod 2105220707:AAGzYOqoUOEwXgupuKseRbVQGG5Y5Tqluv8

// token := "387026696:LIVE:61d30e670f5ef6a30739d8c3"
// token := "371317599:TEST:1638986618188"
// token := "371317599:TEST:1656440886217" // my token
// token := "371317599:TEST:1656442514104" // my token

type Config struct {
	BotToken        string
	PaymentToken    string
	PartnerBotToken string

	HTTP_PORT string
}

// Load ...
func Load() *Config {
	if err := godotenv.Load(); err != nil {
		fmt.Println("No .env file found")
	}

	config := Config{}
	config.BotToken = cast.ToString(getOrReturnDefaultValue("BOT_TOKEN", "2105220707:AAGzYOqoUOEwXgupuKseRbVQGG5Y5Tqluv8"))
	config.PartnerBotToken = cast.ToString(getOrReturnDefaultValue("PARTNER_BOT_TOKEN", "5009480809:AAHP6dwtinjlHTfKk-pHRlHWFd-_dbOPo3M"))

	config.PaymentToken = cast.ToString(getOrReturnDefaultValue("PAYMENT_TOKEN", "387026696:LIVE:61d30e670f5ef6a30739d8c3"))
	config.HTTP_PORT = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":8081"))
	return &config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	val, exists := os.LookupEnv(key)

	if exists {
		return val
	}

	return defaultValue
}
