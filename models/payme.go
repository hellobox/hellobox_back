package models

type CheckPerformTransactionAnswer struct {
	Allow bool `json:"allow"`
}

type PaymeResponse struct {
	Result interface{} `json:"result"`
}

type CreateTransactionAnswer struct {
	CreateTime  int64       `json:"create_time"`
	Transaction string      `json:"transaction"`
	State       int32       `json:"state"`
	Receivers   []Receivers `json:"receivers"`
}

type Receivers struct {
	Id     string `json:"id"`
	Amount int64  `json:"amount"`
}

type PerformTransactionAnswer struct {
	Transaction string `json:"transaction"`
	State       int32  `json:"state"`
	PerformTime int64  `json:"perform_time"`
}

type CancelTransactionAnswer struct {
	Transaction string `json:"transaction"`
	State       int32  `json:"state"`
	CancelTime  int64  `json:"cancel_time"`
}

type CheckTransactionAnswer struct {
	Transaction string `json:"transaction"`
	State       int32  `json:"state"`
	CreateTime  int64  `json:"create_time"`
	PerformTime int64  `json:"perform_time"`
	CancelTime  int64  `json:"cancel_time"`
	Reason      *int64 `json:"reason"`
}

type PaymeRequest struct {
	Method string `json:"method"`
	Params Params `json:"params"`
	Id     int32  `json:"id"`
}

type Account struct {
	OrderId string `json:"order_id"`
}

type Params struct {
	Id      string  `json:"id"`
	Amount  int64   `json:"amount"`
	From    int64   `json:"from"`
	To      int64   `json:"to"`
	Account Account `json:"account"`
	Time    int64   `json:"time"`
	Reason  int32   `json:"reason"`
}

type Transaction struct {
	PaycomId    string `json:"paycom_id" gorm:"primaryKey"`
	Time        int64  `json:"time"`
	Amount      int64  `json:"amount"`
	OrderId     int32  `json:"order_id"`
	CreateTime  int64  `json:"create_time"`
	PerformTime int64  `json:"perform_time"`
	CancelTime  int64  `json:"cancel_time"`
	State       int32  `json:"state"`
	Reason      int64  `json:"reason"`
	Transaction string `json:"transaction"`
	UserId      uint   `json:"user_id"`
}

