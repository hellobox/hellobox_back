package models

import "time"

type Invoice struct {
	Id        string `gorm:"primary_key"`
	UserId    uint   `gorm:"not null"`
	ChatId    int64  `gorm:"not null"`
	Status    string `gorm:"not null"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

