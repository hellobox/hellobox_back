package main

import (
	"hellobox/bot"
	"hellobox/config"
	"hellobox/database"
	"hellobox/env"
	"hellobox/router"
	"net/http"

	"github.com/gorilla/handlers"
)

func main() {
	cfg := config.Load()
	go bot.HandleBot(cfg)
	go bot.HandlePartnerBot(cfg)
	database.InitialMigration()
	router.CreateRouter()
	http.ListenAndServe(cfg.HTTP_PORT, handlers.CORS(handlers.AllowedHeaders([]string{"X-Requested-With", "Access-Control-Allow-Origin", "Content-Type", "Authorization", "Token"}), handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS"}), handlers.AllowedOrigins([]string{"*"}))(env.Router))
}
