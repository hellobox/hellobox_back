package controller

import (
	"encoding/json"
	"hellobox/database"
	"hellobox/models"
	"net/http"
)

func GetOrders(w http.ResponseWriter, r *http.Request) {
	var status string

	url := r.URL.Query()

	status = url.Get("status")

	category := database.GetOrders(status)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(category)
}


func UpdateOrder(w http.ResponseWriter, r *http.Request) {
	var order models.OrderStatus

	json.NewDecoder(r.Body).Decode(&order)

	database.EditOrder(models.Order{Id: order.Id}, order.Status)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(order)
}

