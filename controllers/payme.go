package controller

import (
	"encoding/json"
	"fmt"
	"hellobox/bot"
	"hellobox/database"
	"hellobox/env"
	"hellobox/models"
	"hellobox/payme"
	"net/http"
	"strconv"
	"time"
)

func response(w http.ResponseWriter, r *http.Request, answer interface{}) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(answer)
}

func Payme(w http.ResponseWriter, r *http.Request) {
	var request models.PaymeRequest

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		response(w, r, payme.InternalServerErrorException(request.Id))
		return
	}

	var (
		answer interface{}
		ok     bool
	)

	var order_id int32
	if request.Params.Account.OrderId != "" {
		id, err := strconv.Atoi(request.Params.Account.OrderId)
		if err != nil {
			fmt.Printf("\n\n\nOrder %d not found, err: %+v\n", id, err)
			response(w, r, payme.OrderNotExistsException(request.Id))
			return
		}

		order_id = int32(id)
	}

	switch request.Method {
	case "CheckPerformTransaction":
		answer, ok = CheckPerformTransaction(request.Id, order_id, request.Params.Amount)
	case "CreateTransaction":
		answer, ok = CreateTransaction(request.Id, request.Params.Id, request.Params.Time, request.Params.Amount, order_id)
	case "CheckTransaction":
		answer, ok = CheckTransaction(request.Id, request.Params.Id)
	case "PerformTransaction":
		answer, ok = PerformTransaction(request.Id, request.Params.Id)
	case "CancelTransaction":
		answer, ok = CancelTransaction(request.Id, request.Params.Id, int64(request.Params.Reason))
	case "GetStatement":
		answer, ok = GetState(request.Params.From, request.Params.To)
	default:
		answer = payme.MethodNotFound(request.Id)
	}

	if !ok {
		fmt.Printf("\n\n\n\n\n\n\n\nPAYME_ERROR: answer: %v, request: %+v\n\n\n\n", answer, request)

		response(w, r, answer)
		return
	}

	response(w, r, answer)
}

func CheckPerformTransaction(request_id, order_id int32, amount int64) (interface{}, bool) {
	order := database.GetOrderById(uint(order_id))

	if order.Id < 1 || order.Status == "" {
		fmt.Printf("\n\n\nOrder %d not found\n\n\n", order_id)
		return payme.OrderNotExistsException(request_id), false
	}

	if order.Status == "paid" {
		fmt.Printf("\n\n\nOrder %d already paid\n\n\n", order_id)
		return payme.OrderNotExistsException(request_id), false
	}

	if order.Amount*100 != amount {
		fmt.Printf("\n\n\nOrder %d amount %d != %d\n", order_id, order.Amount, amount)
		return payme.WrongAmountException(request_id), false
	}

	transaction := database.GetTransactionByOrderId(order_id)
	if transaction.PaycomId != "" || transaction.OrderId < 1 {
		fmt.Printf("\n\n\nTransaction %d not found (checkPerformTransaction)\n\n\n", order_id)
		return payme.CheckPerformTransactionAnswer(true), true
	}

	if transaction.State == payme.STATE_IN_PROGRESS {
		fmt.Printf("\n\n\nTransaction %d in progress\n\n\n", order_id)
		return payme.OrderNotExistsException(request_id), false
	}

	return payme.InternalServerErrorException(request_id), false
}

func CreateTransaction(request_id int32, paycom_id string, time_ int64, amount int64, order_id int32) (interface{}, bool) {
	transaction := database.GetTransactionByPaycomId(paycom_id)

	order := database.GetOrderById(uint(order_id))
	if order.Id < 1 || order.Status == "" {
		fmt.Printf("\n\n\nOrder %d not found\n\n\n", order_id)
		return payme.OrderNotExistsException(request_id), false
	}

	if order.Status == "paid" {
		fmt.Printf("\n\n\nOrder %d already paid\n\n\n", order_id)
		return payme.OrderNotExistsException(request_id), false
	}

	receivers := []models.Receivers{}
	commission := (float64(amount) / float64(100+order.Commission)) * float64(order.Commission)

	receivers = append(receivers, models.Receivers{
		Amount: amount - int64(commission),
		Id:     env.TELEGRAM_MERCHANT_ID,
	})

	receivers = append(receivers, models.Receivers{
		Amount: int64(commission),
		Id:     env.HELLOBOX_MERCHANT_ID,
	})

	if transaction.PaycomId != "" {
		fmt.Printf("\n\n\nTransaction %s already exists\n\n\n", paycom_id)

		if transaction.State == payme.STATE_IN_PROGRESS {
			if time.Now().Unix()-transaction.CreateTime > payme.TIME_EXPIRES {
				fmt.Printf("\n\n\nTransaction %s expired\n\n\n", paycom_id)
				return payme.UnableCompleteException(request_id), false
			}

			return payme.CreateTransactionAnswer(transaction.CreateTime, transaction.Transaction, transaction.State, receivers), true
		}
	} else {
		status, ok := CheckPerformTransaction(request_id, order_id, amount)
		if !ok {
			return status, false
		}

		createTime := time.Now().Unix()
		transaction := time.Now().Format("2006-01-02 15:04:05")

		if paycom_id == "" {
			return payme.OrderNotExistsException(request_id), false
		}

		database.CreateTransaction(models.Transaction{
			PaycomId:    paycom_id,
			Amount:      amount,
			OrderId:     order_id,
			State:       payme.STATE_IN_PROGRESS,
			CreateTime:  createTime,
			UserId:      order.UserId,
			Transaction: transaction,
		})

		return payme.CreateTransactionAnswer(createTime, transaction, payme.STATE_IN_PROGRESS, receivers), true
	}

	return payme.InternalServerErrorException(request_id), false
}

func PerformTransaction(request_id int32, paycom_id string) (interface{}, bool) {
	transaction := database.GetTransactionByPaycomId(paycom_id)

	if transaction.PaycomId == "" {
		fmt.Printf("\n\n\nTransaction %s not found\n\n\n", paycom_id)
		return payme.TransactionNotFoundException(request_id), false
	}

	if transaction.State == payme.STATE_DONE {
		return payme.PerformTransactionAnswer(transaction.PaycomId, transaction.State, transaction.PerformTime), true
	}

	if transaction.State == payme.STATE_IN_PROGRESS {
		fmt.Printf("\n\n\nTransaction %s in progress\n\n\n", paycom_id)

		transaction.PerformTime = time.Now().Unix()

		if time.Now().Unix()-transaction.CreateTime > payme.TIME_EXPIRES {
			fmt.Printf("\n\n\nTransaction %s expired\n\n\n", paycom_id)

			transaction.State = payme.STATE_CANCELED
			database.UpdateTransaction(transaction)
			return payme.UnableCompleteException(request_id), false
		} else {
			transaction.State = payme.STATE_DONE
			database.UpdateTransaction(transaction)

			// UPDATE ORDER AND SEND STATUS
			bot.SuccessfulPayment(transaction.OrderId)

			return payme.PerformTransactionAnswer(transaction.PaycomId, transaction.State, transaction.PerformTime), true
		}
	} else {
		return payme.UnableCompleteException(request_id), false
	}
}

func CancelTransaction(request_id int32, paycom_id string, reason int64) (interface{}, bool) {
	transaction := database.GetTransactionByPaycomId(paycom_id)

	if transaction.PaycomId == "" {
		fmt.Printf("\n\n\nTransaction %s not found\n\n\n", paycom_id)
		return payme.TransactionNotFoundException(request_id), false
	}

	transaction.Reason = reason

	if transaction.State == payme.STATE_DONE {
		transaction.State = payme.STATE_POST_CANCELED
	} else {
		transaction.State = payme.STATE_CANCELED
	}

	if transaction.CancelTime == 0 {
		transaction.CancelTime = time.Now().Unix()
	}

	database.UpdateTransaction(transaction)

	// TODO: UPDATE ORDER AND SEND STATUS

	return payme.CancelTransactionAnswer(transaction.Transaction, transaction.State, transaction.CancelTime), true
}

func CheckTransaction(request_id int32, paycom_id string) (interface{}, bool) {
	transaction := database.GetTransactionByPaycomId(paycom_id)

	if transaction.PaycomId == "" {
		fmt.Printf("\n\n\nTransaction %s not found\n\n\n", paycom_id)
		return payme.TransactionNotFoundException(request_id), false
	}

	var reason *int64 = nil

	if transaction.Reason > 0 {
		reason = &transaction.Reason
	}

	return payme.CheckTransactionAnswer(transaction.Transaction, transaction.State, transaction.CreateTime, transaction.PerformTime, transaction.CancelTime, reason), true
}

func GetState(from, to int64) (interface{}, bool) {
	result := database.GetState(from, to, payme.STATE_DONE)

	fmt.Printf("\n\n\nGetState %d %d %+v\n\n\n", from, to, result)

	return payme.TransactionsAnswer(result), true
}

