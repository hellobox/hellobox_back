package env

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/gorilla/mux"
)

var (
	Bot        *tgbotapi.BotAPI
	PartnerBot *tgbotapi.BotAPI
	Router     *mux.Router

	// merchants
	HELLOBOX_MERCHANT_ID string = "601a97d6c16ad418fad8017a"
	TELEGRAM_MERCHANT_ID string = "61d30e670f5ef6a30739d8c3"
	// MERCHANT_ID string = "601404cc5a08a5dda9445aec"
)
