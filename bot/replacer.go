package bot

import "strings"

type Pair struct {
	First  string
	Second string
}

func replaceCharacters(s string) string {
	s = strings.ReplaceAll(s, "\\", "")

	arr := []Pair{
		{
			First: `_`, Second: "\\_",
		},
		// {
		// 	First: `*`, Second: "\\*",
		// },
		// {
		// 	First: `[`, Second: "\\[",
		// },
		// {
		// 	First: `]`, Second: "\\]",
		// },
		// {
		// 	First: `(`, Second: "\\(",
		// },
		// {
		// 	First: `)`, Second: "\\)",
		// },
		{
			First: `~`, Second: "\\~",
		},
		{
			First: `#`, Second: "\\#",
		},
		{
			First: `+`, Second: "\\+",
		},
		{
			First: `-`, Second: "\\-",
		},
		{
			First: `=`, Second: "\\=",
		},
		{
			First: `{`, Second: "\\{",
		},
		{
			First: `}`, Second: "\\}",
		},
		{
			First: `.`, Second: "\\.",
		},
		{
			First: `!`, Second: "\\!",
		},
		// {
		// 	First: `|`, Second: "\\|",
		// },
	}

	for _, v := range arr {
		s = strings.ReplaceAll(s, v.First, v.Second)
	}

	return s
}

