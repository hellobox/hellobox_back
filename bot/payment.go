package bot

import (
	"fmt"
	"hellobox/database"
	"hellobox/env"
	"hellobox/models"
	"hellobox/payme"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func MakeOrderForPayme(update *tgbotapi.Update) {
	chatId := update.CallbackQuery.From.ID
	messageId := update.CallbackQuery.Message.MessageID

	user := database.FilterUser(models.User{TgId: chatId})

	txt := "***Ваш заказа:***"
	if user.Cart == nil || len(user.Cart.Products) <= 0 {
		msg := tgbotapi.NewMessage(chatId, fmt.Sprintf("%s\nВаша корзинка пустая", txt))
		msg.ParseMode = "markdown"
		env.Bot.Send(msg)
		env.Bot.Send(tgbotapi.NewDeleteMessage(chatId, messageId))
		return
	}

	for _, el := range user.Cart.Products {
		txt = fmt.Sprintf("%s\n***%s***\n└  %s  %d x %d = %d", txt, el.Product.Name, el.Product.Name, el.Count, el.Product.Price, el.Product.Price*el.Count)
	}
	percent := database.GetProfitPercent()
	total := float64(user.Cart.CartTotal())
	commission := float64(user.Cart.CartTotal()*percent.Percent) / 100.0

	txt = fmt.Sprintf("%s\n\nКомиссия: %d%%\n\nВсего: %0.0f сум", txt, percent.Percent, total+commission)
	txt += "\n\nСпасибо за ваш заказ."
	txt += "\n\nОплата через Payme"
	txt += "\n\nЧтобы оплатить нажмите на кнопку \"✅ Оплатить\".\n"

	database.CreateOrder(models.Order{
		Cart:       *user.Cart,
		UserId:     uint(user.Id),
		Status:     "unpaid",
		Amount:     int64(total + commission),
		Commission: percent.Percent,
	})

	order := database.GetOrderForPayment(user.Id)
	fmt.Printf("\n\n\norder: %+v\n\n\n\n", order)

	markup := tgbotapi.NewInlineKeyboardMarkup()
	link := payme.GeneratePaymeLink(int32(order.Id), order.Amount, env.TELEGRAM_MERCHANT_ID)

	markup.InlineKeyboard = append(markup.InlineKeyboard, tgbotapi.NewInlineKeyboardRow(tgbotapi.NewInlineKeyboardButtonURL("✅ Оплатить", link)))

	del := tgbotapi.NewDeleteMessage(chatId, messageId)
	env.Bot.Send(del)

	msg := tgbotapi.NewMessage(chatId, txt)
	msg.ParseMode = "markdown"
	msg.ReplyMarkup = markup

	env.Bot.Send(msg)
}

func SuccessfulPayment(order_id int32) {
	fmt.Printf("\n\n\n\n\n\nsuccessfulPayment\n\n\n\n\n\n")

	order := database.GetOrderById(uint(order_id))
	database.EditOrder(models.Order{Id: order.Id}, "paid")

	user := database.GetUserById(order.UserId)

	//Sending prompt
	txt := "Ваш заказ выполнен ✅\nМожете переслать изображение продукта с кодом любому человеку 😊"
	prompt := tgbotapi.NewMessage(user.ChatId, txt)
	env.Bot.Send(prompt)

	//Sending tokens
	for _, el := range user.Cart.Products {
		//Product data
		selectedOption := el.OptionIndex
		url := el.Product.ImageUrl
		if selectedOption != 0 {
			if selectedOption == 1000 {
				pr := database.GetPresentImage()
				url = pr.ImageUrl
			} else {
				url = el.Product.Options[selectedOption-1].ImageUrl
			}
		}
		//Sending photo
		file := tgbotapi.NewPhoto(user.TgId, tgbotapi.FileURL(url))
		file.ParseMode = "MarkdownV2"
		text := fmt.Sprintf("***%s***\n%s", el.Product.Name, replaceCharacters(el.Product.Description))
		if el.OptionIndex == 1000 {
			text = "***Вам отправили подарок 🎁***\nЧтобы его открыть переходите  по ссылке в Телеграм бот «Hellobox \\(https://t\\.me/helloboxbot\\)»\\.И напишите «Код активации» в раздел: \n***«🎁 Открыть полученный подарок»\\.***"
		}

		t := time.Now()
		from := t.Format("02\\.01\\.2006")
		t = t.AddDate(0, 0, int(el.Product.ExpiresIn))
		to := t.Format("02\\.01\\.2006")

		txt := fmt.Sprintf("⏰Период активации: %s\\-%s\n 🐼 Количество:%d\n🔑Код активации:  ||***%s***||\n\\_\\*Не показывайте код другим людям, его можете активировать только вы или знающий его человек;\n\\*Сервис пока работает только на территории города Ташкента\\_", from, to, el.Count, el.Token)
		file.Caption = text + "\n" + txt
		env.Bot.Send(file)
	}

	fmt.Printf("Successful payment :%d\n\n", order_id)

	database.ClearUserCart(user)
}

