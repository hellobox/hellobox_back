package database

import "hellobox/models"

func CreateInvoice(invoice models.Invoice) {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	connection.Create(&invoice)
}

func GetInvoices() []models.Invoice {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	var invoice []models.Invoice
	connection.Order("CreatedAt DESC").Find(&invoice)
	return invoice
}

func GetInvoiceById(id string) models.Invoice {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	var invoice models.Invoice
	connection.Where(models.Invoice{Id: id}).First(&invoice)
	return invoice
}

func UpdateInvoice(invoice models.Invoice) {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	connection.Save(&invoice)
}

