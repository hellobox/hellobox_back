package database

import "hellobox/models"

func CreateTransaction(transaction models.Transaction) {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	connection.Create(&transaction)
}

func GetTransactionByOrderId(orderId int32) models.Transaction {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	var transaction models.Transaction
	connection.Where(models.Transaction{OrderId: orderId}).First(&transaction)
	return transaction
}

func GetTransactionByPaycomId(paycomId string) models.Transaction {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	var transaction models.Transaction
	connection.Where(models.Transaction{PaycomId: paycomId}).First(&transaction)
	return transaction
}

func UpdateTransaction(transaction models.Transaction) {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	connection.Save(&transaction)
}

func GetState(from, to int64, state int32) []models.Transaction {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	var transactions []models.Transaction
	connection.Where("create_time >= ? AND create_time <= ? AND state = ? ", from, to, state).Find(&transactions)
	return transactions
}

