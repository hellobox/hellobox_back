package database

import (
	"hellobox/models"
	"strings"
)

func GetUsers() []models.User {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	var users []models.User
	connection.Find(&users)
	return users
}

func GetPartnerUsers() []models.User {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	var users []models.User
	connection.Where(models.User{FromPartner: 1}).Find(&users)
	return users
}

func GetUser(userId uint) models.User {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	user := models.User{Id: userId}
	connection.Where(&user).Preload("Cart").Find(&user)
	return user
}

func FilterUser(usr models.User) models.User {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	connection.Where(&usr).Preload("Cart").Preload("Cart.Products").Preload("Cart.Products.Product").Preload("Cart.Products.Product.Options").Find(&usr)
	return usr
}

func CreateUser(user models.User) *models.Error {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	var (
		usr   models.User
		users []models.User
	)

	user.Phone = strings.TrimPrefix(user.Phone, "+")

	usr.Phone = user.Phone
	connection.Where(usr).Find(&users)

	if len(users) > 1 {
		for _, el := range users {
			DeleteUser(el.Id)
		}
	} else if len(users) == 1 {
		usr = users[0]
	}

	if usr.TgId != 0 {
		var err models.Error = models.Error{IsError: true, Message: "User already exist"}
		return &err
	}

	connection.Create(&user)
	return nil
}

func EditUser(user models.User) {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	connection.Save(&user)
	if user.Cart != nil {
		connection.Save(&user.Cart)
		if len(user.Cart.Products) > 0 {
			for _, el := range user.Cart.Products {
				connection.Save(&el)
			}
		}
	}
}

func ClearUserCart(user models.User) {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	connection.Model(&user).Association("Cart").Delete(user.Cart)
}

func DeleteUser(id uint) {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	connection.Delete(&models.User{Id: id})
}

func GetUserById(id uint) models.User {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	var user models.User
	connection.Where(models.User{Id: id}).Preload("Cart").Preload("Cart.Products").Preload("Cart.Products.Product").Preload("Cart.Products.Product.Options").Find(&user)
	return user
}
