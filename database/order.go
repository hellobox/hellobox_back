package database

import "hellobox/models"

func GetOrderHistory(userId uint) []models.Order {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	var orders []models.Order
	connection.Where(models.Order{UserId: userId}).Preload("Cart").Preload("Cart.Products").Preload("Cart.Products.Product").Preload("Cart.Products.Product.Options").Preload("Cart.Products.Product.Category").Find(&orders)
	return orders
}

func GetOrders(status string) []models.Order {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	var orders []models.Order
	if status != "" {
		connection.Where(models.Order{Status: status}).Order("id desc").Preload("Cart").Preload("User").Preload("Cart.Products").Preload("Cart.Products.Product").Preload("Cart.Products.Product.Options").Preload("Cart.Products.Product.Category").Find(&orders)
	} else {
		connection.Where(models.Order{Status: "paid"}).Order("id desc").Preload("Cart").Preload("User").Preload("Cart.Products").Preload("Cart.Products.Product").Preload("Cart.Products.Product.Options").Preload("Cart.Products.Product.Category").Find(&orders)
	}

	return orders
}

func CreateOrder(orders models.Order) {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	connection.Create(&orders)
}

func GetOrderByUserId(userId uint) models.Order {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	var orders models.Order
	connection.Where(models.Order{UserId: userId}).Order("id desc").Limit(1).Preload("Cart").Preload("Cart.Products").Preload("Cart.Products.Product").Preload("Cart.Products.Product.Options").Preload("Cart.Products.Product.Category").Find(&orders)
	return orders
}

func GetOrderById(id uint) models.Order {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	var orders models.Order
	connection.Where(models.Order{Id: id}).Preload("Cart").Preload("Cart.Products").Preload("Cart.Products.Product").Preload("Cart.Products.Product.Options").Preload("Cart.Products.Product.Category").Find(&orders)
	return orders
}

func EditOrder(orders models.Order, status string) {
	connection := GetDatabase()
	defer CloseDatabase(connection)

	connection.Model(&orders).Select("status").Updates(map[string]interface{}{"status": status})
}


func DeleteOrder(id uint) {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	connection.Delete(&models.Order{Id: id})
}

func GetOrderForPayment(userId uint) models.Order {
	connection := GetDatabase()
	defer CloseDatabase(connection)
	var orders models.Order
	connection.Where(models.Order{UserId: userId}).Order("id desc").Limit(1).Find(&orders)
	return orders
}

